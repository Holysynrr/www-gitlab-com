---
layout: markdown_page
title: "Functional Group Updates"
---

## Format

Functional Group Updates are regular updates from a group at GitLab.

1. We'll use Zoom for now, will switch to YouTube Live later.
1. Presentations are optional, if you use them please ensure everyone with the url can access (inside and outside GitLab).
1. All calls except sales and finance are public unless mentioned otherwise, in the future all calls will be public.
1. Right now everyone at GitLab the company is invited to each call, we'll invite the wider community later.
1. Attendence is optional.
1. Video will be published on our blog so contributers, users, and customers can see it. We'll probably publish a week of recordings in one go with [automated transcriptions](https://www.labnol.org/internet/transcribe-video-to-text/28914/)
1. The update is also announced on and the recording linked from our team call agenda.
1. Tone should be informal, like explain to a friend what happend in the group last month, it shouldn't require a lot of presentation.
1. You can invite someone in the team to give it, it doesn't need to be the team lead, but the team lead is responsible that it is given.
1. Calls are 4 times a week 30 minutes before the team call, 8:00 to 8:25am Pacific.
1. Calls do not go over time, if there are more questions invite them via chat, if there are no more questions feel free to end the call after 5 minutes.
1. Calls are scheduled by an [EA](https://about.gitlab.com/jobs/executive-assistant/) who can also switch team based on availability of the presenter.
1. Every group with 5 people or more presents.

## Schedule

There is a rotating schedule:

Week 1:

- Monday Discussion Sean
- Tuesday Product Job
- Wednesday   PeopleOps Joan
- Thursday    Sales   Chad

Week 2:

- Monday  Backend Douwe
- Tuesday Infrastructure  Pablo
- Wednesday   General Sid
- Thursday    Finance Paul

Week 3:

- Monday  UX   Allison
- Tuesday CI  Kamil
- Wednesday   Build   Marin
- Thursday Support Ernst

Week 4:

- Monday  Frontend    Jacob Schatz
- Tuesday Alliances Michael
- Wednesday Prometheus Ben
- Thursday Engineering Stan

Week 5:

- Monday  Marketing   Tim
- Tuesday Edge Remy
- Wednesday Partnerships Eliran
- More marketing groups

## Agenda

A possible agenda for the call is:

1. Accomplishments
1. Concerns
1. Stalled/need help
1. Plans
1. Questions in chat

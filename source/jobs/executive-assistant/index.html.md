---
layout: job_page
title: "Executive Assistant"
---

## Primary responsibilities

* The EA is an assistant to the entire executive team
* Handle email in executive's name including triage.
* Prepare draft responses to emails based on experience or verbal instructions.
* Heavy calendar management
* Make changes to the handbook based on verbal instructions.
* Ensure executive's appointments are confirmed a day in advance and that the attendees have access to and are aware of the relevant materials (about.gitlab.com/primer, google doc, etc.).
* Take care of expenses.
* Take care of travel, lodging, and lunch/dinner reservations.
* Sign up for services, make changes to them, and close them.
* Coordinate internal and external events, summits, conferences and off-sites.
* Suggest more efficient processes and procedures.
* Office management related tasks.
* Get a lot of feedback on how to handle above tasks and encode them in the handbook.

## Other responsibilities
* Research applicants.
* Do screening calls with applicants.
* Draft contracts.
* Stage contracts in our software.
* Take care of contract paperwork.
* Help out with Dutch translations and interactions.
* Handle paperwork for immigration related tasks.
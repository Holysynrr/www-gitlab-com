---
title: "Frontend functional group update"
author: Sean Packham
author_twitter: SeanPackham
categories: gitlab
image_title:
description: 'Watch the recording from our Frontend team update.'
cta_button_text: Watch the <strong>8.16 release webcast</strong> live!
cta_button_link: https://page.gitlab.com/20170126_autodeploy_autodeploywebterminal.html
---

Jacob Schatz, GitLab's Frontend Lead, covers some of the awesome things the GitLab team and community have created and gives a preview of what is to come in future releases.

<!-- more -->

<figure>
  <iframe width="800" height="480" src="https://www.youtube.com/embed/YlYY_AyBPOc" frameborder="0" allowfullscreen></iframe>
</figure>
